<?php 
	session_start();
	include 'admin/includes/conn.php';

if(!isset($_SESSION["id"])&& empty($_SESSION["id"])) // If session is not set then redirect to Login Page
    {
        header("Location:user_logout.php");  
    }
        //secho $_SESSION["id"];

        echo "Welcome ";
		echo $_SESSION["name"];
        //echo "<a href='user_logout.php'> Logout</a> "; 

?>
<?php
  include 'timezone.php';
  $range_to = date('m/d/Y');
  $range_from = date('m/d/Y', strtotime('-30 day', strtotime($range_to)));
?>
<html lang="en">	
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="../../favicon.ico">
    <title>Penyata Bayaran Pencen</title>
    <!-- Latest compiled and minified CSS -->
	   
  <!--[if lt IE 10]>       
      <script type="text/javascript">var ie = 9;</script>      
  <![endif]-->     
  <!--[if lt IE 9]>                   
      <script type="text/javascript">var ie = 8;</script>          
  <![endif]-->  
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha256-Av2lUNJFz7FqxLquvIFyxyCQA/VHUFna3onVy+5jUBM= sha512-zyqATyziDoUzMiMR8EAD3c5Ye55I2V3K7GcmJDHbL49cXzFQCEA6flSKqxEzwxXqq73/M4A0jKFFJ/ysEhbExQ==" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">   
      
    
  
  </head>
<body>
<div class="row hidden-print">
	<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-body">
			<a class="btn btn-danger" href="user_logout.php">Log Keluar</a>
		  </div>
		</div>
	</div>
</div>
	<div class="container logged">
     
<div class="container">
<div class="row">
	<div class="col-md-12" align="centre">
		<div class="panel panel-info hidden-xs">
		  <div class="panel-body">
		    <img class="img-responsive" alt="Jabatan Perkhidmatan Awam" src="felda banner.jpg">
		  </div>
		</div>
	</div>
</div>

<div class="row">		 
	<div class="col-md-12">
			</div>
</div>

<div class="row" >
	<div class="col-md-12">
		<div class="panel panel-info">
		  <div class="panel-heading" style="background-color:#2F4F4F">Maklumat Peribadi</div>
			  
			  	<table class="table table-bordered">
				
			  		<tbody><tr>
				<?php
					
					$conn = new mysqli('localhost', 'root', '', 'apsystem');

					if ($conn->connect_error) {
					die("Connection failed: " .$conn->connect_error);
					}	
						$empid1 = $_SESSION["id"];
						$sql = "SELECT employee_id , firstname , lastname , icnumber, address FROM employees WHERE employee_id='$empid1'";
						$query = $conn->query($sql);
						$row = $query->fetch_assoc();
						$nopek = $row['employee_id'];
						$firstnama = $row['firstname'];
						$lastnama = $row['lastname'];
						$icnumber = $row['icnumber'];
					
			  			echo "
						<td class='text-center'><b>Nama</b><br>
			  				".$row['firstname']." "	.$row['lastname']."<br>

						</td>
						<tr>
						<td class='text-center'><b>Alamat</b><br>".$row['address']."</td>
						</tr>
						</tr>	
						<tr>
			  			<td class='text-center'><b>Tarikh Dicetak</b><br>". date("d/m/Y") ."<br></td>
						</tr>
						<tr>
							<td class='text-center'><b>No Kad Pengenalan</b><br>$icnumber</td>
						</tr>
						<tr>
							<td class='text-center'><b>No Pekerja</b><br>$nopek</td>
						</tr>"
					
					?>
			  	</tbody>
				
				</table>
				
		</div>		
	</div>
</div>

<div class="row">

	<div class="col-md-12">
	
		<div class="panel panel-info"><div class="box-header with-border">
             
            </div>
			
		  <div class="panel-heading" style="background-color:#2F4F4F">Penyata Bayaran Tahunan <?php print($_SESSION['tahun']); ?></div>
		   <div class="pull-right">
				
              
					
        </div>
		<div class="panel-body">
		<div class="table-responsive">
		    <table class="table table-bordered">
				<thead>
				<tr> 
					<th>Slip Gaji Bulanan</th>
					<th>Bulan</th>
				     <th>Gaji Pokok(RM)</th>
					 <th>Elaun(RM)</th>
				     <th>Gaji Kasar(RM)</th>
				     <th>Potongan(RM)</th>
					 <th>Gaji Bersih(RM)</th>
				  </tr>
				</thead>
					<?php
					$year = $_SESSION['tahun'];	
					if(isset($_GET['month'])){
                      $month = $_GET['month'];
                      $ex = explode('-', $month);
                      $from = date('Y-m-d', strtotime($ex[0]));
                      $to = date('Y-m-d', strtotime($ex[1]));
					  $monthnow = date("F",strtotime($from));
					  $monthnow2 = date("",strtotime($from));
						
                    }
					
					//deductions
					$sql = "SELECT *, SUM(amount) as total_amount FROM deductions ";
                    $query = $conn->query($sql);
                    $drow = $query->fetch_assoc();
                    $deduction = $drow['total_amount'];	
					
					//earnings
					$sql = "SELECT *, SUM(amount) as total_amount FROM earnings ";
                    $query = $conn->query($sql);
                    $erow = $query->fetch_assoc();
                    $earnings = $erow['total_amount'];	

					//basic salary
					$sql = "SELECT employee_id, basic_salary
							FROM employees
							WHERE employee_id='$empid1'";
					
					$query = $conn->query($sql);
					$row = $query->fetch_assoc();		
					$basicsalary = $row['basic_salary'];
					$gross = $basicsalary+ $earnings;
					
					//info
					$psql ="SELECT p.employee_id, p.month, p.deduction, p.earning,p.final_salary, e.firstname,e.basic_salary, e.lastname FROM penyata as p
							LEFT JOIN employees as e ON p.employee_id=e.employee_id 
							WHERE tahun='$year' AND e.employee_id='$empid1' GROUP BY month 
							";
					
					$pquery = $conn->query($psql)or die($conn->error);
					$total = 0;
					while ($prow = $pquery->fetch_assoc()){	
					
					
					$gross = $prow['basic_salary'] + $prow['earning'];
					$final = $gross-$prow['deduction'];
					$monthp = $prow['month'];
					$monthName = date('F', mktime(0, 0, 0, $monthp, 10));
					$firstname = $prow['firstname'];
                    $net = $gross - $deduction;
					$total += $final;
				
					echo"
							<tbody>
							<tr> 
								<td>
									<form id='printForm' action='payslip_generate_emp.php'>
									
										<input type='hidden' name='empid1' id='empid1' value='".$row['employee_id']."'>
										<input type='hidden' name='month' id='daterange1' value='$monthp'>
										
									<button type='submit' id='empid1'> 
									<span class='glyphicon glyphicon-print'></span> Payslip </button>
									</form>
								</td>
							    <td class='text-left'>$monthName</td>
								<td class='text-left'>".number_format($prow['basic_salary'], 2)."</td>
								<td class='text-left'>".number_format($prow['earning'], 2)."</td>
								<td class='text-left'>".number_format($gross, 2)."</td>
								<td class='text-left'>".number_format($prow['deduction'], 2)."</td>
								<td class='text-left'>".number_format($final, 2)."</td>
								
							</tr>
							
							</tbody>
						
							<tfoot>
					";
					}
						echo"
						<tr>           
						<td colspan='6' align='right'><b>Jumlah &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</b></td>
						<td class='text-left'>".number_format($total, 2)."</td>
						</tr>";
						?>
				</tfoot>
			</table>
		  </div>
		  </div>
		</div>
	</div>
</div>

<div class="row hidden-print">
	<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-body"> 
		  <form id='printForm' method='GET' action='payroll_generate.php'>
							<input type='hidden' name="month" id="daterange1" value='<?php echo $_GET['month']?>'>
								<button class="btn btn-success type="submit" id="submitpdf">Cetak PDF</button> 
								<a class="btn btn-danger" href="user_logout.php">Log Keluar</a>
			</form>
			
		  </div>
		</div>
	</div>
</div>
</div>
	</div>


	<footer class="footer">
      <div class="container">
        <p class="text-muted text-center hidden-print">© Copyright 2019 All Rights Reserved. Cyberark Limited.</p>
      </div>
    </footer>

  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script>

		$(function(){
			$("#dateform").on('change', function(){
			var range = encodeURI($(this).val());
			window.location = 'payroll.php?user_page='+range;
			});

		};
    </script>


</body></html>