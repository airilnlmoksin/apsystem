<div class="container containerpadding">
       
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default	">			  
			  <div class="panel-body">
			    <form method="POST" action="">			    	
			    	<div class="text-center hidden-xs"><img src="https://apps.jpapencen.gov.my/images/logo-jpa.png"></div>					
					<hr class="hidden-xs">
					<div class="captionbg">
						<h3 class="text-center text-capitalize"><strong>Penyata Bayaran Pencen</strong></h3>
						<h5 class="text-center text-capitalize">Semakan Penyata Pencen bagi Pesara Kerajaan Malaysia.Sila isi maklumat berikut</h5>
					</div>			    	
			    	<hr>
			    				 		<input type="hidden" name="_token" value="G4YHzRMtvU4FPF6DzyvYlL8rPdBgifYejOPTsV72">
					<div class="form-group">		
						<label class="sr-only" for="noKp">No Kad Pengenalan:</label>
						<div class="input-group">							
							<div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
							<input type="text" name="noKp" id="noKp" class="form-control" value="" placeholder="No Kad Pengenalan tanpa (-)">
						</div>
					</div>	
					<div class="form-group">	
						<label class="sr-only" for="noAccPencen">No Akaun Pencen:</label>
						<div class="input-group">							
							<div class="input-group-addon"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></div>
							<input type="text" name="noAccPencen" id="noAccPencen" class="form-control text-uppercase" value="" placeholder="No Akaun Pencen">
						</div>
					</div>
					<div class="form-group">
					  <label class="sr-only" for="sel1">Tahun</label>
					  <div class="input-group">
					  	<div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
					  <select class="form-control" id="sel1" name="tahun">

					  	 
					  	{ 
					  		<option value="2019">2019</option>
					  	}
					  	 
					  	{ 
					  		<option value="2018">2018</option>
					  	}
					  	 
					  	{ 
					  		<option value="2017">2017</option>
					  	}
					  	 
					  	{ 
					  		<option value="2016">2016</option>
					  	}
					  	 
					  	{ 
					  		<option value="2015">2015</option>
					  	}
					  	 
					  	{ 
					  		<option value="2014">2014</option>
					  	}
					  	 
					  	{ 
					  		<option value="2013">2013</option>
					  	}
					  	 
					  	{ 
					  		<option value="2012">2012</option>
					  	}
					  	 
					  	{ 
					  		<option value="2011">2011</option>
					  	}
					  	 
					  	{ 
					  		<option value="2010">2010</option>
					  	}
					  						    					    
					  </select>
					</div>
					</div>
					<hr>
					<button type="submit" class="btn btn-info btn-block">Hantar</button>				
				</form>
			  </div>
			</div>
		</div>
	</div>	

   
	</div>