<?php
	session_start();
	include 'includes/conn.php';

	if(isset($_POST['login'])){
		$empid = $_POST['empid'];
		$id = $_POST['id'];

		$sql = "SELECT * FROM employees WHERE employee_id = '$empid'";
		$query = $conn->query($sql);

		if($query->num_rows < 1){
			$_SESSION['error'] = 'Cannot find account with the username';
		}
		else{
			$row = $query->fetch_assoc();
			if(password_verify($id, $row['id'])){
				$_SESSION['employees'] = $row['id'];
			}
			else{
				$_SESSION['error'] = 'Incorrect password';
			}
		}

	}
	/**else{
		$_SESSION['error'] = 'Input employee id first';
	}

	header('location: user_print.php');

?>