<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<head>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include 'includes/navbar.php'; ?>
  <?php include 'includes/menubar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
        }
		if(isset($_SESSION['exist'])){
          echo "
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Fail!</h4>
              ".$_SESSION['exist']."
            </div>
          ";
          unset($_SESSION["exist"]);
        }
      ?>
      <h1 align="left">
        Create Payslip
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Payslip</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
		<?php 			

			if(isset($_GET['month'])){
			$month1= date('m', strtotime($_GET['month']));
			$csql = "SELECT id FROM penyata WHERE tahun='$_GET[year]' AND month = '$month1' AND employee_id= '$_GET[empid1]' ";
			$result = mysqli_query($conn, $csql);
			$row_cnt = mysqli_num_rows($result);

			if ($row_cnt>0&& isset($_GET['empid1'])){
			$_SESSION['exist']='Record exist! (This will overwrite previous payslip data)';
				}
			}
		?>
      <?php
        if(isset($_SESSION['error'])){
          echo "
            <div class='alert alert-danger alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Error!</h4>
              ".$_SESSION['error']."
            </div>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              ".$_SESSION['success']."
            </div>
          ";
          unset($_SESSION['success']);
		}
		  if(isset($_SESSION['exist'])){
          echo "
            <div class='alert alert-warning alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-warning'></i> Fail!</h4>
              ".$_SESSION['exist']."
            </div>
          ";
          unset($_SESSION['exist']);
        }
        
      ?>
      <!-- Add -->
<div class="row">

    <form class="well form-horizontal" action="#" method="get"  id="contact_form">
	<fieldset>

<!-- Form Name -->
<!--legend><center><h2><b>Create Payslip</b></h2></center></legend><br>

<!-- Text input-->

	<div class="x_content" >

		<div class="col-md-3 col-xs-7">
            <div class="form-group" class= "row align-items-center">
                <label class="control-label" style="margin-bottom: 5px;">Employee Name</label>	
				<select  name="empid1" class="form-control">
					<?php 
						$sql = "SELECT basic_salary, employee_id, firstname, lastname FROM employees";
						$query = $conn->query($sql);
						while ($row = $query->fetch_assoc()){
						echo "<option value=".$row['employee_id'].">" . $row['firstname']." ".$row['lastname']." (".$row['employee_id'].")</option>";
						}
						?>
				</select>	
            </div>
		</div>
		<div class="col-md-2 col-xs-6">
            <div class="form-group">
                <label class="control-label" style="margin-bottom: 5px;">Year</label>
                <select name="year" class="form-control" required>
                    <option value="" selected >Select Year</option>
                    	    	    
					<option value = "2014"  > 2014 </option>	
	    	    
					<option value = "2015"  > 2015 </option>
	    	    
					<option value = "2016"  > 2016 </option>
	    	    
					<option value = "2017"  > 2017 </option>
	    	    
					<option value = "2018"  > 2018 </option>
	    	    
					<option value = "2019"  > 2019 </option>

					<option value = "2020"  > 2020 </option>
					
					<option value = "2021"  > 2021 </option>
                </select>
            </div>
		</div>
		<div class="col-md-2 col-xs-6">
            <div class="form-group">
                <label class="control-label" style="margin-bottom: 5px;">Month</label>
                <select name="month" class="form-control" required>
			<option value="" selected >Select Month</option>
			<option value = "January" >January</option>
			<option value = "February" >February</option>	
			<option value = "March" >March</option>	
			<option value = "April" >April</option>	
			<option value = "May" >May</option>	
			<option value = "June" >June</option>	
			<option value = "July" >July</option>	
			<option value = "August" >August</option>	
			<option value = "September" >September</option>	
			<option value = "October" >October</option>	
			<option value = "November"  >November</option>	
			<option value = "December" >December</option>      
                 </select>
            </div>
        </div>

  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
 
  
  </div>
  </div>
</div>	
<!-- Success message -->
<!--/*<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>*/

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-primary" onsubmit="div_hide()">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-chevron-right"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
  </div>
</div>	
</fieldset>
</form>
</div>
<div class="form-popup" id="myForm1" <?php if( !isset($_GET['empid1']) || isset($_SESSION['exist']) ){ echo "style='display:none'"; } ?>">

<form class="form-horizontal" action="add_payslip.php" method="post" id="myForm">
<div class="row">

<!--Year and month value to be passed to add_payslip.php -->
<input type="hidden" value="<?php echo $_GET['year'];?>" name="year">
<input type="hidden" value="<?php echo $_GET['month'];?>" name="month">
<input type="hidden" value="<?php echo $_GET['empid1'];?>" name="empid1">
<?php if(isset($_GET['empid1'])){ echo "<h4><font color=blue> &nbsp;&nbsp; ($_GET[empid1]) $_GET[month], $_GET[year]</font></h4>";}?>

	  <div class="col-md-6">
	  	<div class="x_panel">
                  <div class="x_title">
                    <h4> Allowances</h4>

                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content"><br>
				  



<div class="row" id="allowance_list">
<div class="form-group">

    <div class="col-sm-4 col-xs-5 col-sm-offset-1">
        <select name="allowance[]" id="allowance_1" class="form-control">
            <option value="" selected="">Allowance</option>			    
				<option value="Elaun Kedatangan">Elaun Kedatangan</option>
				<option value="Elaun Perumahan">Elaun Perumahan</option>
				<option value="Elaun Sara Hidup">Elaun Sara Hidup</option>
				<option value="Elaun Makan">Elaun Makan</option>
				<option value="Elaun Pengangkutan">Elaun Pengangkutan</option>
				<option value="Bonus">Bonus</option>

        </select>
    </div> 

    <div class="col-sm-4 col-xs-5">
        <input type="number" min="0" step="any" class="form-control has-feedback-left" name="a_amount[]" id="a_amount_1" value="" onchange="calculate(this.id);" placeholder="Amount"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
    </div>

    <div class="col-sm-2 col-xs-2">
        <button type="button" class="btn btn-default" name="a_add" id="a_add" onclick="add_allowance(this)">
        <i class="glyphicon glyphicon-plus"></i></button>
    </div>  

</div>
</div> 


<div class="row" id="a_hidden" style="display: block;">
<div class="form-group">

    <div class="col-sm-4 col-xs-5 col-sm-offset-1">
        <select name="allowance[]" id="allowance" class="form-control">
             <option value="" selected="">Allowance</option>			    
				<option value="Elaun Kedatangan">Elaun Kedatangan</option>
				<option value="Elaun Perumahan">Elaun Perumahan</option>
				<option value="Elaun Sara Hidup">Elaun Sara Hidup</option>
				<option value="Elaun Makan">Elaun Makan</option>
				<option value="Elaun Pengangkutan">Elaun Pengangkutan</option>
				<option value="Bonus">Bonus</option>>
        </select>
    </div>

    <div class="col-sm-4 col-xs-5">
        <input type="number" min="0" step="any" class="form-control has-feedback-left" name="a_amount[]" id="a_amount" value="" onchange="calculate(this.id);" placeholder="Amount"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
    </div>
    <div class="col-sm-2 col-xs-2">
        <button type="button" class="btn btn-default" name="a_delete" id="a_delete" onclick="delete_allowance(this)">
        <i class="glyphicon glyphicon-trash"></i></button>
    </div>  
</div>
</div> 


<div class="row">
<div class="col-sm-4 col-sm-offset-4">
        <button type="button" class="btn btn-info btn-sm" name="a_add" id="a_add" onclick="add_allowance(this)">
        <i class="glyphicon glyphicon-plus"></i>Add Allowance</button>
</div> 
</div>

<hr>

<h5>Other Allowances</h5><br>

        <div class="form-group">
        <div class="col-sm-offset-1 col-sm-5 col-xs-5">
                <input type="text" class="form-control" name="other_allowance" id="type1" placeholder="Title">
        </div>    
        <div class="col-sm-5 col-xs-5">
                <input type="number" class="form-control has-feedback-left" name="other_a_amount" id="amount1" placeholder="Amount" onchange="check1(this.id);"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
        </div>
        </div>
		
		</div>
		</div>
	  </div>
	  	
	<div class="col-md-6">
	  	<div class="x_panel">
                  <div class="x_title">
                    <h4> Deductions</h4>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content"><br>



		

<div class="row" id="deduction_list">
<div class="form-group">

    <div class="col-sm-4 col-xs-5 col-sm-offset-1">
        <select name="deduction[]" id="deduction_1" class="form-control">
            <option value="" selected="">Deduction</option>			    
				<option value="EPF" >EPF</option>
				<option value="KWSP">KWSP</option>
				<option value="PERKESO">PERKESO</option>
				<option value="SIP">SIP</option>
				<option value="Ketidakhadiran">Ketidakhadiran</option>
				<option value="Gaji Pendahuluan">Gaji Pendahuluan</option>
        </select>
    </div> 

    <div class="col-sm-4 col-xs-5">
        <input type="number" min="0" step="any" class="form-control has-feedback-left" name="d_amount[]" id="d_amount_1" value="" onchange="calculate(this.id);" placeholder="Amount"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
    </div>

    <div class="col-sm-2 col-xs-2">
        <button type="button" class="btn btn-default" name="d_add" id="d_add" onclick="add_deduction(this)">
        <i class="glyphicon glyphicon-plus"></i></button>
    </div>  

</div>
</div>

<div class="row" id="d_hidden" style="display: none;">
<div class="form-group">

    <div class="col-sm-4 col-xs-5 col-sm-offset-1">
        <select name="deduction[]" id="deduction" class="form-control">
            <option value="" selected="">Deduction</option>			    
				<option value="EPF" >EPF</option>
				<option value="KWSP">KWSP</option>
				<option value="PERKESO">PERKESO</option>
				<option value="SIP">SIP</option>
				<option value="TDS">TDS</option>
				<option value="Advance Salary">Advance Salary</option>
        </select>
    </div> 

    <div class="col-sm-4 col-xs-5">
        <input type="number" min="0" step="any" class="form-control has-feedback-left" name="d_amount[]" id="d_amount" value="" onchange="calculate(this.id);" placeholder="Amount"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
    </div>
    <div class="col-sm-2 col-xs-2">
        <button type="button" class="btn btn-default" name="d_delete" id="d_delete" onclick="delete_deduction(this)">
        <i class="glyphicon glyphicon-trash"></i></button>
    </div>  
</div>
</div> 

<div class="row">
<div class="col-sm-4 col-sm-offset-4">
        <button type="button" class="btn btn-info btn-sm" name="d_add" id="d_add" onclick="add_deduction(this)">
        <i class="glyphicon glyphicon-plus"></i>Add Deduction</button>
</div> 
</div>

		

<hr>

<h5>Other Deductions</h5><br>

    <div class="form-group">
        <div class="col-sm-offset-1 col-md-5 col-xs-5">
                <input type="text" class="form-control" name="other_deduction" id="type2" placeholder="Title">
        </div>
        <div class="col-md-5 col-xs-5">
                <input type="number" class="form-control has-feedback-left" name="other_d_amount" id="amount2" placeholder="Amount" onchange="check2(this.id);"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
        </div>
    </div>
			  	
		</div>
		</div>
	  </div>
</div>


<div class="row">

<div class="col-sm-offset-1 col-md-10">
<div class="x_panel">
                  <div class="x_title">
                    <h2> Summary </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                       
	 
<div class="form-group">
	<label for="basic" class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Basic</label>
	<div class="col-sm-6 col-xs-12">
<?php
$empid1=$_GET['empid1'];
	$sql = "SELECT basic_salary FROM employees 
			WHERE employee_id='$empid1'";
			
	$query = $conn->query($sql);
	$row = $query->fetch_assoc();
	$basic_salary = $row['basic_salary'];
	
	echo"
	<input type='text' name='basic' id='basic' class='form-control has-feedback-left' value='$basic_salary' readonly=''><span class='fa fa-usd form-control-feedback left' aria-hidden='true'></span>
	"
?>

	</div>
</div>
	
<div class="form-group">
	<label for="allowance" class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Total Allowances</label>
	<div class="col-sm-6 col-xs-12">
	    <input type="text" name="total_allowance" id="total_allowance" class="form-control has-feedback-left" value="" readonly=""><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
		<input type="hidden" name="basic_salary" id="basic" value="<?php echo($basic_salary); ?>">

	</div>	
</div>

<div class="form-group">
	<label for="inputEmail3" class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Total Deduction</label>
	<div class="col-sm-6 col-xs-12">
	    <input type="text" name="total_deduction" id="total_deduction" class="form-control has-feedback-left"  value="" readonly=""><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
		

	</div>
</div>							  

<div class="form-group">
	<label for="inputEmail3" class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Net Salary</label>
	<div class="col-sm-6 col-xs-12">
	    <input type="text" name="total" id="net" class="form-control has-feedback-left" value="" readonly=""><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>


	</div>
</div>

<div class="form-group">
	<label class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Payment Method</label>
	<div class="col-sm-6 col-xs-12">
		<select class="form-control" name="payment_mode">
		<option value="">Select</option>
		<option value="1">Cash</option>
		<option value="2">Bank</option>
		<option value="3">Other</option>
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Status</label>
	<div class="col-sm-6 col-xs-12">
		<select class="form-control" name="status">
		<option value="1">Paid</option>
		<option value="0">Unpaid</option>
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-offset-1 col-sm-2 col-xs-12 control-label">Comments</label>
	<div class="col-sm-6 col-xs-12">
	    <textarea type="text" name="comments" class="form-control" row="3"></textarea>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-4 col-sm-4">
	    <button type="submit" class="btn btn-success" id="submit_button" name="create" value="4">
                    <i class="entypo-check"></i> Create Payslip </button>
	</div>
</div>
		
</div>
</div>
</div>
</div>

</form>
    </div>
	</div>
	<!-- /.container -->
    </section>   
  </div>
    
  <?php include 'includes/footer.php'; ?>
  <?php include 'includes/deduction_modal.php'; ?>
</div>
<?php include 'includes/scripts.php'; ?>

	
<script type="text/javascript">

function check1(str)
{
	var x = document.getElementById("type1");
	if(x.value=="")
	{	
		alert("Enter the allowance type first");
		document.getElementById(str).value =  "";
		exit();
	}

	calculate(str);
}

function check2(str)
{
	var x = document.getElementById("type2");
	if(x.value=="")
	{
		alert("Enter the deduction type first");
		document.getElementById(str).value =  "";
		exit();
	}

	calculate(str);
}

function total1()
{
	var x = document.getElementById("amount1").value;
	x=Number(x);
	var y = document.getElementById("Dallowance").value; 
	y=Number(y);
	y+= x;
	document.getElementById("allowance").value=y;

	var z = document.getElementById("net").value; 
	z=Number(z);
	z+= x;
	document.getElementById("net").value=z;
}

function total2()
{
	var x = document.getElementById("amount2").value;
	x=Number(x);
	var y = document.getElementById("Ddeduction").value; 
	y=Number(y);
	y+= x;
	document.getElementById("deduction").value=y;

	var z = document.getElementById("net").value; 
	z=Number(z);
	z-= x;
	document.getElementById("net").value=z;
}


</script>
</script>
<script type="text/javascript">

var a_item_count  = 6;
var a_max_count = 6;

var d_item_count = 3;
var d_max_count = 3;

$("#a_hidden").hide("fast");
$("#d_hidden").hide("fast");

// CREATING BLANK ALLOWANCE INPUT
    var blank_item = '';
    $(document).ready(function () {
        a_blank_item = $('#a_hidden').html();
        d_blank_item = $('#d_hidden').html();
    });


function add_allowance()
{
    a_item_count++;
	a_max_count++;
	$("#allowance_list").append(a_blank_item);
        $('#allowance').attr('id', 'allowance_' + a_max_count);
        $('#a_amount').attr('id', 'a_amount_' + a_max_count);
        $('#a_delete').attr('id', 'a_delete_' + a_max_count);
}

    // REMOVING ALLOWANCE INPUT
function delete_allowance(n) 
{
        a_item_count--;
    	var m = $(n).parent().parent("div");
        $(m).remove();
       // deleted_allowances.push(item_count);

	calculate(n.id);

}

function add_deduction()
{
        d_item_count++;
	d_max_count++;
        $("#deduction_list").append(d_blank_item);
        $('#deduction').attr('id', 'deduction_' + d_max_count);
        $('#d_amount').attr('id', 'd_amount_' + d_max_count);
        $('#d_delete').attr('id', 'd_delete_' + d_max_count);
}

    // REMOVING ALLOWANCE INPUT
function delete_deduction(n, allowance_count) 
{
        d_item_count--;
    	var m = $(n).parent().parent("div");
        $(m).remove();
       // deleted_allowances.push(item_count);

	calculate(n.id);
}

function calculate(str)
{
	var n = str.lastIndexOf('_');
	var s = str.substring(n);
	var name = str.substring(0,n);
	var allowance = "allowance" + s;
	var deduction = "deduction" + s;

	if(name == "a_amount") 
	{
		if(document.getElementById(allowance).value == '')
		{
			alert("Enter the allowance type first");
			document.getElementById(str).value =  "";
			exit();
		}
	}
	else if(name == "d_amount") 
	{
		if(document.getElementById(deduction).value == '')
		{
			alert("Enter the deduction type first");
			document.getElementById(str).value =  "";
			exit();
		}
	}

	var a_total=0,d_total=0;var i;var a;var d;var n1,n2,m1,m2;
	for(i=1;i<=a_max_count;i++)
	{	
		a="a_amount_"+i;
		if(!document.getElementById(a)) continue;
		n1 = document.getElementById(a).value;
		m1 = Number(n1);
		a_total+=m1;
	}

	var b1 = Number(document.getElementById("basic").value);
	var a1 = Number(document.getElementById('amount1').value);
	var d1 = Number(document.getElementById('amount2').value);

	document.getElementById("total_allowance").value =  (a_total + a1).toFixed(2);
	for(i=1;i<=d_max_count;i++)
	{	
		d="d_amount_"+i;
		if(!document.getElementById(d)) continue;
		n2 = document.getElementById(d).value;
		m2 = Number(n2);
		d_total+=m2;				
	}
	document.getElementById("total_deduction").value =  (d_total + d1).toFixed(2);

	document.getElementById("net").value =  (b1 + a1 +a_total - d1 - d_total).toFixed(2);

}
</script>
<script>
	function openForm() {
        document.getElementById("myForm").style.display = "block";
        };
		
	function closeForm() {
        document.getElementById("myForm").style.display = "none";
        document.getElementById("empid0").value="";
        document.getElementById("leave").value="";
        document.getElementById("sel").value="";

        }
	function div_hide(){
	var x = document.getElementById("myForm");
  
		if (x.style.display === "none") {
		x.style.display = "block";
	} 	
		else {
		x.style.display = "none";
	}
}

	}
</script>
</body>
</html>
