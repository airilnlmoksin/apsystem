<?php
	include 'includes/session.php';
	
   
	$range = $_POST['date_range'];
	$ex = explode(' - ', $range);
	$from = date('Y-m-d', strtotime($ex[0]));
	$to = date('Y-m-d', strtotime($ex[1]));

	$sql = "SELECT *, SUM(amount) as total_amount FROM deductions";
    $query = $conn->query($sql);
   	$drow = $query->fetch_assoc();
    $deduction = $drow['total_amount'];

	$from_title = date('d M, Y', strtotime($ex[0]));
	$to_title = date('d M, Y', strtotime($ex[1]));

	require_once('../tcpdf/tcpdf.php');  
	require_once('tcpdf_include.php');
	//create new PDF doc
    $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
	//insert image		
	$width = $pdf->getPageWidth();
	$height = $pdf->getPageHeight();

	
	//set doc information
    $pdf->SetCreator(PDF_CREATOR);  
    $pdf->SetTitle('Payslip: '.$from_title.' - '.$to_title);  
	
	// set default header data	
    $pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING); 

	// set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA)); 
	
	// set default monospaced font
    $pdf->SetDefaultMonospacedFont('dejavusans');  
	
	// set margins
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);
    $pdf->setPrintHeader(false);  
    $pdf->setPrintFooter(false);  
    $pdf->SetAutoPageBreak(TRUE, 10);  
    $pdf->SetFont('freesans', '', 11);	
    $pdf->AddPage(); 
	$contents = '';
	
	$sql = "SELECT *, SUM(num_hr) AS total_hr, attendance.employee_id AS empid, employees.employee_id AS employee FROM attendance 
			LEFT JOIN employees ON employees.id=attendance.employee_id 
			LEFT JOIN position ON position.id=employees.position_id 
			WHERE date BETWEEN '$from' AND '$to' 
			GROUP BY attendance.employee_id 
			ORDER BY employees.lastname ASC, employees.firstname ASC";

	$query = $conn->query($sql);
	while($row = $query->fetch_assoc()){
		$empid = $row['empid'];
                      
      	$casql = "SELECT *, SUM(amount) AS cashamount FROM cashadvance WHERE employee_id='$empid' 
				 AND date_advance BETWEEN '$from' AND '$to'";
      
      	$caquery = $conn->query($casql);
      	$carow = $caquery->fetch_assoc();
      	$cashadvance = $carow['cashamount'];

		$gross = $row['rate'] * $row['total_hr'];	
		$total_deduction = $deduction + $cashadvance;
  		$net = $gross - $total_deduction;
		
		$contents .='
		<img src="https://pngimage.net/wp-content/uploads/2018/05/company-logo-png-8.png" 
				style="width:68px;height:58px;float:right;">
				
			<table border="0">
			
				<h2 align="center">CYBERARK MALAYSIA</h1>
				<p align="center"><small>No 3, Jalan Merpati 2, Taman Burung, 82000, Pulai, Johor</small></p>	
				
			</table>
			';	
		
		
		
		$contents .= '
		
			<h4 align="center">'.$from_title." - ".$to_title.'</h4>
			
			<table border="0">  
				
    	       	<tr>
					
            		<td width="20%" align="left" >Employee Name: </td>
                 	<td width="25%"><b>'.$row['firstname']." ".$row['lastname'].'</b></td>
					<td></td>
					<td></td>
				 	
    	    	</tr>
    	    	<tr >
					
    	    		<td width="20%" align="left">Employee ID: </td>
				 	<td width="25%">'.$row['employee'].'</td>
					<td></td>
					<td></td>

    	    	</tr>
			</table>
			
			<table border="0.5" height="">
				
				<tr>
					<td align="left" bgcolor="lightgrey"><b>Earning</b></td>
					<td align="right" bgcolor="lightgrey"><b>Amount</b></td>
					<td align="left" bgcolor="lightgrey"><b>Deduction</b></td>
					<td align="right" bgcolor="lightgrey"><b>Amount</b></td>
				</tr>
    	    	<tr> 
    	    		
					<td align="left">Rate per Hour: </td>
                 	<td align="right">'.number_format($row['rate'], 2).'</td>
					<td align="left">Deduction </td>
				 	<td align="right">'.number_format($deduction, 2).'</td>
				 	
    	    	</tr>
    	    	<tr> 
    	    		
					<td align="left">Total Hours: </td>
				 	<td align="right">'.number_format($row['total_hr'], 2).'</td>
					<td align="left">Cash Advance: </td>
				 	<td align="right">'.number_format($cashadvance, 2).'</td> 
				 	
    	    	</tr>
    	    	<tr> 
    	    		
					<td align="left"><b>Gross Pay: </b></td>
				 	<td align="right"><b>'.number_format(($row['rate']*$row['total_hr']), 2).'</b></td>
					<td align="left"><b>Total Deduction:</b></td>
				 	<td align="right"><b>'.number_format($total_deduction, 2).'</b></td>
				 	
    	    	</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="left"><b>Net Pay:</b></td>
				 	<td align="right"><b>'.number_format($net, 2).'</b></td> 
				</tr>
				
    	    </table>
			<br><br><br>
			';
		

	}
    $pdf->writeHTML($contents);
    $pdf->Output('payslip.pdf', 'I');
	
?>	