<?php
ob_start();
	$conn = new mysqli('localhost', 'root', '', 'apsystem');

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}
	
	  include 'timezone.php';
		$datenow = date('d/m/Y');
?>
<?php
	session_start();

	if(!isset($_SESSION['id']) || trim($_SESSION['id']) == ''){
		header('location: user_print.php');
	}
	
	$sql = "SELECT e.employee_id, y.tahun FROM employees AS e, year as y 
			WHERE employee_id='" . $_SESSION["id"] . "' and tahun = '".$_SESSION['tahun']."'";
	$query = $conn->query($sql);
	$user = $query->fetch_assoc();
	$empid1 = $_SESSION['id'];
	$tahun = $_SESSION['tahun'];
?>		
<?php

	$conn = new mysqli('localhost', 'root', '', 'apsystem');

	if ($conn->connect_error) {
	    die("Connection failed: "  .$conn->connect_error);
		
	}								
		$empid1 = $_SESSION["id"];
		$sql = "SELECT employee_id , firstname , lastname , icnumber FROM employees WHERE employee_id='$empid1'";
		$query = $conn->query($sql);
		$row = $query->fetch_assoc();
		$nopek = $row['employee_id'];
		$firstnama = $row['firstname'];
		$lastnama = $row['lastname'];
		$icnumber = $row['icnumber'];

	function generateRow($conn, $deduction){
		
		$contents = '';
		$empid1 = $_SESSION["id"];
		$sql = "SELECT employee_id , firstname , lastname , icnumber FROM employees 
				WHERE employee_id='$empid1'";
		$query = $conn->query($sql);
		$row = $query->fetch_assoc();
		$nopek = $row['employee_id'];
		$firstnama = $row['firstname'];
		$lastnama = $row['lastname'];
		$icnumber = $row['icnumber'];
		$tahun = $_SESSION['tahun'];
			
					
		$psql ="SELECT p.employee_id, p.month, p.deduction, p.earning, p.basic_salary,p.final_salary, e.firstname, e.lastname FROM penyata as p
							LEFT JOIN employees as e ON p.employee_id=e.employee_id 
							WHERE tahun='$tahun' AND e.employee_id='$empid1' GROUP BY month
							";
					
		$pquery = $conn->query($psql) or die($conn->error);
		$total = 0;
		while($prow = $pquery->fetch_assoc()){
			
			$final = $prow['final_salary'];
					$gross = $prow['basic_salary'] + $prow['earning'];
					$firstname = $prow['firstname'];
					$deduction = $prow['deduction'];
					$earning = $prow['earning'];
					$monthNum = $prow['month'];
                    $net = $gross - $deduction;
					$total += $net;
			$contents .= '
			<tr>
				<td>'.date('F', mktime(0, 0, 0, $monthNum, 10)).'</td>
				<td>'.number_format($prow['basic_salary'], 2).'</td>
				<td>'.number_format($earning, 2).'</td>
				<td>'.number_format($gross, 2).'</td>
				<td>'.number_format($deduction, 2).'</td>
				<td align="right">'.number_format($net, 2).'</td>
			</tr>
			';
		}																			
		$contents .= '	
			<tr>
				<td colspan="5" align="right"><b>Jumlah</b></td>
				<td align="right"><b>'.number_format($total, 2).'</b></td>
			</tr>
		';
		return $contents;		
	}
	$tahun = $_SESSION['tahun'];

	$sql = "SELECT *, SUM(amount) as total_amount FROM deductions";
    $query = $conn->query($sql);
   	$drow = $query->fetch_assoc();
    $deduction = $drow['total_amount'];


	require_once('tcpdf/tcpdf.php');  
    $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
    $pdf->SetCreator(PDF_CREATOR);  
    $pdf->SetTitle('Payroll: '.$tahun.'');  
    $pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
    $pdf->SetDefaultMonospacedFont('helvetica');  
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);  
    $pdf->setPrintHeader(false);  
    $pdf->setPrintFooter(false);  
    $pdf->SetAutoPageBreak(TRUE, 10);  
    $pdf->SetFont('helvetica', '', 11);  
    $pdf->AddPage(); 	
    $content = '';  

    $content .= '	
		<img src="logofelda.jpg" 
				style="float:right;width:60px;height:58px;">
		<table border="0" align="center">
				<h2 align="center">Koperasi Permodalan FELDA Malaysia Berhad</h2>
				<p align="center"><small>Aras Bawah, Balai Felda 1, Jalan Pesiaran Gurney, 54000 Kuala Lumpur,
				<br>Federal Territory of Kuala Lumpur</small>				
				</p>
				<br><br><h4 align="center">Payslip for '.$monthName." ".$tahun.'</h4>
		</table>
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><b>Nama Peneroka:</b> '.$firstnama.' 	'.$lastnama.'</td>
			<td><b>ID Peneroka:</b> '.$empid1.'</td>
			
		</tr>
		<tr>
			<td>
				<p align="left"><b>No K/P:</b> '.$icnumber.'</p>
				</td>
				<td>
				<p align="left"><b>Tarikh Dicetak:</b> '.$datenow.'</p>
				
			</td>
		
		</tr>
		</table>
			
      	<table border="1" cellspacing="0" cellpadding="3">  
           <tr bgcolor="lightgrey"> 
				<th width="18%" align="center"><b>Bulan(RM)</b></th>
                <th width="19%" align="center"><b>Gaji Pokok(RM)</b></th>
				<th width="12%" align="center"><b>Elaun(RM)</b></th>
				<th width="18%" align="center"><b>Gaji Kasar(RM)</b></th>
				<th width="12%" align="center"><b>Potongan (RM)</b></th>
				<th width="21%" align="center"><b>Gaji Bersih(RM)</b></th>
           </tr>
      ';  
    $content .= generateRow($conn, $deduction);  
    $content .= '</table>';  
	ob_end_clean();
    $pdf->writeHTML($content);  
    $pdf->Output('payroll.pdf', 'I');
	