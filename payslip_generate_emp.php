
<?php
	$conn = new mysqli('localhost', 'root', '', 'apsystem');

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}
?>
<?php
	session_start();
	
	if(!isset($_SESSION['id']) || trim($_SESSION['id']) == ''){
		header('location: user_print.php');
	}
	
	$sql = "SELECT e.employee_id, y.tahun FROM employees AS e, year as y 
			WHERE employee_id='" . $_SESSION["id"] . "' and tahun = '".$_SESSION['tahun']."'";
	$query = $conn->query($sql);
	$user = $query->fetch_assoc();
	$empid1 = $_SESSION['id'];
	$tahun = $_SESSION['tahun'];
	include 'timezone.php';
	$datenow = date('d/m/Y');
?>		
	
<?php
	//maklumat pekerja dan gaji basic
	$sql = "SELECT employee_id ,firstname, lastname, basic_salary, icnumber FROM employees 
			WHERE employee_id = '$empid1'";
	$query = $conn->query($sql);
	$row = $query->fetch_assoc();
	$basicsalary = $row['basic_salary'];
	$firstname = $row['firstname'];
	$lastname = $row['lastname'];
	$icnumber = $row['icnumber'];
	
	//date range	
	$monthNum = $_GET['month'];
	$monthName = date('F', mktime(0, 0, 0, $monthNum, 10));	

	//range date
	//$from_title = date('F Y', strtotime($ex[0]));
	

	require_once('tcpdf/tcpdf.php');  
	require_once('admin/tcpdf_include.php');
	//create new PDF doc
    $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
	//insert image		
	$width = $pdf->getPageWidth();
	$height = $pdf->getPageHeight();

	//set doc information
    $pdf->SetCreator(PDF_CREATOR);  
    $pdf->SetTitle("Payslip for $monthName");  
	
	// set default header data	
    $pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING); 

	// set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA)); 
	
	// set default monospaced font
    $pdf->SetDefaultMonospacedFont('freesans');  
	
	// set margins
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
    $pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);
    $pdf->setPrintHeader(false);  
    $pdf->setPrintFooter(false);  
    $pdf->SetAutoPageBreak(TRUE, 10);  
    $pdf->SetFont('freesans', '', 11);	
    $pdf->AddPage();
	$contents = '';
	
	$contents .='
		<img src="logofelda.jpg"
				style="width:60px;height:58px;float:right;">		
		<table border="0">
			<h2 align="center">Koperasi Permodalan FELDA Malaysia Berhad</h1>
			<p align="center"><small>Aras Bawah, Balai Felda 1, Jalan Persiaran Gurney,54000 Kuala Lumpur,
				<br>Federal Territory of Kuala Lumpur</small></p>
			<p></p>
		</table>
			';
		////////////////////////////////////////////////////////////////////////////////////////
		$dsql = "SELECT d.*, penyata_id, p.employee_id, SUM(d.e_earnings) as e_sum ,SUM(d.e_deductions) as d_sum FROM penyata as p, description as d 
				 WHERE p.employee_id= '$empid1' AND d.tahun='$tahun' AND p.month='$monthNum' AND p.penyata_id=d.p_id";
		$dquery  = $conn->query($dsql);
		$drow = $dquery->fetch_assoc();
		/////////////////////////////////////////////////////////////////////////////////////////
		$contents .= '


			<h4 align="center">Payslip for '.$monthName." ".$tahun.'</h4>
			
			<table border="0">  
				
    	       	<tr>
					
            		<td width="20%" align="left" >Nama Peneroka: </td>
                 	<td width="30%"><b>'.$firstname." ".$lastname.'</b></td>
					<td width="15%">Tarikh Dicetak:</td>
					<td>'.$datenow.'</td>
				 	
    	    	</tr>
    	    	<tr >
					
    	    		<td width="20%" align="left">ID Peneroka: </td>
				 	<td width="30%">'.$empid1.'</td>
					<td width="15%"> No K/P:</td>
					<td>'.$icnumber.'</td>

    	    	</tr>	

			</table>
			';
		$contents  .='
			<table border="0.5" height="">
				
				<tr  bgcolor="lightgrey">
					<td align="left"><b>Pendapatan</b></td>
					<td align="right"><b>Jumlah</b></td>
					<td align="left"><b>Potongan</b></td>
					<td align="right"><b>Jumlah</b></td>

				</tr>
    	    	<tr> 
    	    		
					<td align="left">Gaji Pokok: </td>
                 	<td align="right">'.number_format($basicsalary, 2).'</td>
					<td align="left"></td>
				 	<td align="right"></td>
				 	
    	    	</tr>
				
			</table>
			';
			//maklumat
		$psql = "SELECT d.*, penyata_id, p.employee_id FROM penyata as p, description as d 
				 WHERE p.employee_id= '$empid1' AND d.tahun='$tahun' AND p.month='$monthNum' AND p.penyata_id=d.p_id";
		$pquery = $conn->query($psql);

		while($prow = $pquery->fetch_assoc()){
			
		$contents  .='
			<table border="0.5" height="">
				
		<tr>		
					<td align="left">'.$prow['earnings'].'</td>
                 	<td align="right">'.number_format(empty($prow['e_earnings']) ? '' : $prow['e_earnings'],2) .'</td>
					<td align="left">'.$prow['deductions'].'</td>
				 	<td align="right">'.number_format(empty($prow['e_deductions']) ? '' : $prow['e_deductions'],2) .'</td>
    	    	</tr>
			</table>
			';
		}

		$gross = $row['basic_salary']+$drow['e_sum'];
		$finalsalary = $gross-$drow['d_sum'];
		$contents .='
			<table border="0.5">
    	    	<tr> 
    	    		
					<td align="left"><b>Jumlah Elaun:</b></td>
				 	<td align="right"><b>'.number_format($drow['e_sum'], 2).'</b></td>
					<td align="left"><b>Jumlah Potongan:</b></td>
				 	<td align="right"><b>'.number_format($drow['d_sum'], 2).'</b></td>
				 	
    	    	</tr>
				<tr>
					<td><b>Gaji Kasar: </b></td>
					<td align="right"><b>'.number_format($gross, 2).'</b></td>
					<td align="left"><b>Net Pay:</b></td>
				 	<td align="right"><b>'.number_format($finalsalary, 2).'</b></td> 
				</tr>	
			</table>
			';

	ob_end_clean();
    $pdf->writeHTML($contents);
    $pdf->Output('payslip.pdf', 'I');

?>	